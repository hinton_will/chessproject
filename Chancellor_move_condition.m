function condition = Chancellor_move_condition(x0, y0, xf, yf, rect)
  
  xdif = xf - x0; ydif = yf - y0;
  
  % Return true if there is no move.
  if (xdif == 0) && (ydif == 0), return, 1; endif
  
  condition = 1; % Initialize condition as being met
  
  % Create vector of steps to the desired move (rook):
  if ((ydif == 0) && (xdif ~= 0)), xvect = zeros(1, xdif); yvect = zeros(1, xdif);
    for i = (abs(xdif)/xdif):(abs(xdif)/xdif):xdif
      xvect(abs(i)) = x0 + i; yvect(abs(i)) = y0; endfor
  endif
  if ((xdif == 0) && (ydif ~= 0)), 
        xvect = zeros(1, ydif); 
        yvect = zeros(1, ydif);
        
        for i = (abs(ydif)/ydif):(abs(ydif)/ydif):ydif
      xvect(abs(i)) = x0; yvect(abs(i)) = y0 + i; endfor
  endif
    
  % Create vector of steps to the desired move (knight):
  if ((abs(xdif) == 2) && (abs(ydif) == 1)), 
    xvect = [(x0 + (xdif / 2)) xf xf]; yvect = [y0 y0 yf]; endif
  if ((abs(ydif) == 2) && (abs(xdif) == 1)), 
    xvect = [x0 x0 xf]; yvect = [(y0 + (ydif / 2)) y0 yf]; endif
  
  % Test if the vector of steps crosses the removed area
  [counter n_moves] = size(xvect);
  while ((condition) && (counter <= n_moves))
    % If this point falls in the removed area, set condition to false.
    % This exits the while loop and the function returns false.
    if ((xvect(counter) >= rect(1,1)) && (xvect(counter) <= rect(1,2)) && ...
      (yvect(counter) >= rect(2,1)) && (yvect(counter) <= rect(2,2)))
      condition = 0;
    endif
    counter = counter + 1;
  endwhile
    
  return, condition
  
endfunction