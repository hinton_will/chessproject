function matrix = Chancellor_move_validation(x0, y0, N)
  
  matrix = zeros(8);
  for i = 1:N
    [x y] = Chancellor_move2(x0, y0);
    matrix(x, y) = matrix(x, y) + 1.0;
  endfor
  
  matrix = matrix / N;
  bar3octave(matrix)
  
  return, matrix;
  
endfunction