This repository contains the code used to run the computations detailed in the presentation.

Note: To replicate the experimental results, you must preface each call to Chancellor_move, Chancellor_move2, and Chancellor_journey with the command rand("seed", 0).