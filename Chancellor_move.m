function [xf, yf] = Chancellor_move(x0, y0)
 
% ------------------------------------------------------------------------------
%% This section calculates all possible moves of the Chancellor.
%  The Chancellor chess piece moves like a rook and a knight.

  time_before = time();

  possible_xmoves = zeros(1, 64);
  possible_ymoves = zeros(1, 64);
  a = x0; b = y0;
  counter = 1;
  
  % Check all rook moves:
  % All moves directly to the right
  while ((a + 1) < 9), counter = counter + 1; a = a + 1;
    possible_xmoves(counter) = a; possible_ymoves(counter) = b;
  endwhile
  a = x0; b = y0;
  % All moves directly to the left
  while ((a - 1) > 0), counter = counter + 1; a = a - 1;
    possible_xmoves(counter) = a; possible_ymoves(counter) = b;
  endwhile
  a = x0; b = y0;
  % All moves directly up
  while ((b + 1) < 9), counter = counter + 1; b = b + 1;
    possible_xmoves(counter) = a; possible_ymoves(counter) = b;
  endwhile
  a = x0; b = y0;
  % All moves directly down
  while ((b - 1) > 0), counter = counter + 1; b = b - 1;
    possible_xmoves(counter) = a; possible_ymoves(counter) = b;
  endwhile
  a = x0; b = y0;
  
  % Check all knight moves:
  % Up two; right one
  if (((a + 2) < 9) && ((b + 1) < 9)), counter = counter + 1;
    possible_xmoves(counter) = a + 2; possible_ymoves(counter) = b + 1; endif
  % Up two; left one
  if (((a + 2) < 9) && ((b - 1) > 0)), counter = counter + 1;
    possible_xmoves(counter) = a + 2; possible_ymoves(counter) = b - 1; endif
  % Down two; right one
  if (((a - 2) > 0) && ((b + 1) < 9)), counter = counter + 1;
    possible_xmoves(counter) = a - 2; possible_ymoves(counter) = b + 1; endif
  % Down two; left one
  if (((a - 2) > 0) && ((b - 1) > 0)), counter = counter + 1;
    possible_xmoves(counter) = a - 2; possible_ymoves(counter) = b - 1; endif
    
  % Up one; right two
  if (((a + 1) < 9) && ((b + 2) < 9)), counter = counter + 1;
    possible_xmoves(counter) = a + 1; possible_ymoves(counter) = b + 2; endif
  % Up one; left two
  if (((a + 1) < 9) && ((b - 2) > 0)), counter = counter + 1;
    possible_xmoves(counter) = a + 1; possible_ymoves(counter) = b - 2; endif
  % Down one; right two
  if (((a - 1) > 0) && ((b + 2) < 9)), counter = counter + 1;
    possible_xmoves(counter) = a - 1; possible_ymoves(counter) = b + 2; endif
  % Down one; left two
  if (((a - 1) > 0) && ((b - 2) > 0)), counter = counter + 1;
    possible_xmoves(counter) = a - 1; possible_ymoves(counter) = b - 2; endif
  
  % All moves diagonally up and to the right
%  while (((a + 1) < 9) && ((b + 1) < 9)), counter = counter + 1;
%    possible_xmoves(counter) = a + 1; possible_ymoves(counter) = b + 1;
%    a = a + 1; b = b + 1;
%  endwhile
%  a = x0; b = y0;
  % All moves diagonally down and to the right
%  while (((a + 1) < 9) && ((b - 1) > 0)), counter = counter + 1;
%    possible_xmoves(counter) = a + 1; possible_ymoves(counter) = b - 1;
%    a = a + 1; b = b - 1;
%  endwhile
%  a = x0; b = y0;
  % All moves diagonally up and to the left
%  while (((a - 1) > 0) && ((b + 1) < 9)), counter = counter + 1;
%    possible_xmoves(counter) = a - 1; possible_ymoves(counter) = b + 1;
%    a = a - 1; b = b + 1;
%  endwhile
%  a = x0; b = y0;
  % All moves diagonally down and to the left
%  while (((a - 1) > 0) && ((b - 1) > 0)), counter = counter + 1;
%    possible_xmoves(counter) = a - 1; possible_ymoves(counter) = b - 1;
%    a = a - 1; b = b - 1;
%  endwhile
%  a = x0; b = y0;
  
  possible_xmoves = possible_xmoves(possible_xmoves ~= 0);
  possible_ymoves = possible_ymoves(possible_ymoves ~= 0);
  [counter, N_moves] = size(possible_xmoves);
  
  time_after = time();
  time_taken = time_after - time_before;
%  printf("%d moves calculated in %f microseconds. \n", 
%    N_moves, time_taken*(10^6));
%%
%-------------------------------------------------------------------------------

selection = floor((N_moves) * rand() + 1);
xf = possible_xmoves(selection);
yf = possible_ymoves(selection);
return, [xf yf];

endfunction
  