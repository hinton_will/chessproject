function probabilities = Chancellor_path(x0, y0, N)
  
  probabilities = zeros(8); probabilities(x0, y0) = probabilities(x0, y0) + 1.0;
  x_steps = zeros(1, N); y_steps = zeros(1, N);
  x_steps(1) = x0; y_steps(1) = y0;
  for i = 2:N
    [x y] = Chancellor_move(x_steps(i - 1), y_steps(i - 1));
    x_steps(i) = x; y_steps(i) = y;
    probabilities(x, y) = probabilities(x, y) + 1.0;
  endfor
  
  probabilities = probabilities / (N + 1);
  
  
  if (N < 100), figure; hold on; plot(x_steps, y_steps, '-ob'); 
    plot(x0, y0, '*g', "markersize", 30); xlim([0 9]); ylim([0 9]);
    plot(x_steps(N), y_steps(N), 'xr', "markersize", 30); hold off; endif
    
  if (N > 99), figure; bar3octave(probabilities); endif
    
  return, probabilities;
  
endfunction