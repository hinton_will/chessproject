function avgsteps = Chancellor_journey(N)
  
  x0 = 1; y0 = 1; xtarget = 8; ytarget = 8;
  n_steps = zeros(1, N);
  
  for i = 1:N
    
    previous_xstep = x0; previous_ystep = y0; x = x0; y = y0;
    
    while ((x ~= xtarget) || (y ~= ytarget))
      
      [x y] = Chancellor_move(previous_xstep, previous_ystep);
      previous_xstep = x; previous_ystep = y;
      n_steps(1, i) = n_steps(1, i) + 1.0;
      
    endwhile
    
  endfor
  
  avgsteps = sum(n_steps) / N
  
  figure; hist(n_steps, 30);
  
  return, avgsteps;
    
  return, probabilities;
  
endfunction